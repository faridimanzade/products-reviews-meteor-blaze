import {
    ProductsReviews
} from './collections';

import {
    Reviews
} from '../reviews/collections';


Meteor.methods({
    addProductsReviewsCount(productId) {
        let count = Reviews.find({
            productId: productId
        }).count();
        return count;
    }
});