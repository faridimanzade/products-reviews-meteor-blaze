import {
    Images
} from './collections';


Meteor.publish('getImages', function () {
    return Images.find().cursor;
});


// import {
//     publishComposite
// } from 'meteor/reywood:publish-composite';


// publishComposite('getImages', {
//     find() {
//         return Images.find().cursor;
//     },
//     children: [{
//         find(user) {
//             return Meteor.users.find({
//                 _id: user.userId
//             });
//         }
//     }]
// });