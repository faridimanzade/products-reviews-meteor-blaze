import {
    Reviews
} from './collections';


Meteor.publish('getLastReviews', function (productId) {
    return Reviews.find({productId: productId}, {
        sort: {
            dateTime: -1
        },
        limit: 3
    });
});