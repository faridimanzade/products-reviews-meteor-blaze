import {Categories} from './collections';


Meteor.publish('getCategories', function () {
    return Categories.find({});
});