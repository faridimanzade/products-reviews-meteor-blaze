import {Products} from './collections';


Meteor.publish('getProducts', function () {
    return Products.find({});
});