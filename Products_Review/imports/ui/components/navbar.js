import {
    Template
} from 'meteor/templating';

import './navbar.html'


Template.navbar.helpers({
    loggedIn() {
        return Meteor.userId();
    }
});

// Template.navbar.events({
//     'click #foo': function (event, template) {

//     }
// });