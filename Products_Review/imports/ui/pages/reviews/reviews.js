import {
    Template
} from 'meteor/templating';
import {
    ReactiveVar
} from 'meteor/reactive-var';
import {
    FlowRouter
} from 'meteor/ostrio:flow-router-extra';

import {
    Reviews
} from '../../../api/reviews/collections'
import {
    Products
} from '../../../api/products/collections'
import {
    Images
} from '../../../api/images/collections'


import './reviews.html'



Template.reviews.onCreated(function () {

    this.productId = new ReactiveVar();
    this.productRating = new ReactiveVar(0);
    this.reviewCount = new ReactiveVar(0);

    this.autorun(() => {
        this.subscribe('getProducts');
        this.subscribe('getImages');
        this.subscribe('getLastReviews', this.productId.get());
        
        FlowRouter.watchPathChange()
        this.productId.set(FlowRouter.getParam('_id'));
    

        Meteor.call('getCount',this.productId.get(), (err,res)=>{
            if(err) alert(err)
            if(res) this.reviewCount.set(res)
          })
    
    });

});



Template.reviews.helpers({
    // =============================================== GET WHETHER USER IS SIGNED IN
    loggedIn(){
        return Meteor.userId();
    },

    // =============================================== GET CURRENT PRODUCT
    getProduct() {
        // Template.instance().productId.set(FlowRouter.getParam('_id'))
        return Products.findOne({
            _id: Template.instance().productId.get()
        });
    },

    // =============================================== GET FULL STARTS OF CURRENT PRODUCT RATING
    getAvgRatingGolden() {
        let product = Products.findOne({
            _id: Template.instance().productId.get()
        });

        if (!product) {
            return;
        }

        Template.instance().productRating.set(product?.avgRating)

        return StarFull(Template.instance().productRating.get())
    },

    // =============================================== GET EMPTY STARTS OF CURRENT PRODUCT RATING
    getAvgRatingEmpty() {
        let product = Products.findOne({
            _id: Template.instance().productId.get()
        });

        if (!product) {
            return;
        }

        return StarFull((5 - Template.instance().productRating.get()))
    },


    // =============================================== GET NUMBER OF REVIEWS
    getReviewsCount() {
        return Template.instance().reviewCount.get();
    },

    // =============================================== GET IMAGE OF CURRENT PRODUCT
    getImage() {
        return Images.findOne({
            _id: this.imageId
        })?.link();
    },

    // =============================================== GET LAST 3 REVIEWS OF CURRENT PRODUCT
    getLastReviews() {
        return Reviews.find({
            productId: Template.instance().productId.get()
        }, {
            sort: {
                dateTime: -1
            },
            limit: 3
        });
    },

    // =============================================== GET TIME FORMAT OF CURRENT REVIEW
    getTime() {
        return moment(this.dateTime).format("MM-DD-YYYY")
    },

    // =============================================== GET FULL STARTS OF CURRENT REVIEW RATING
    getStarFull() {
        let rating = this.rating;
        return StarFull(rating)
    },

    // =============================================== GET FULL STARTS OF CURRENT REVIEW RATING
    getStarEmpty() {
        let rating = this.rating;
        return StarFull((5 - rating))
    }
});



// =========================== RETURNS ARRAY OF STARS OF RATING
function StarFull(rating = 0) {
    let arr = new Array(rating);

    for (let index = 0; index < rating; index++) {
        arr[index] = 1;
    }

    return arr;
}