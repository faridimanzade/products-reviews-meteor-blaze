import {
  Template
} from 'meteor/templating';
import {
  ReactiveVar
} from 'meteor/reactive-var';
import {
  FlowRouter
} from 'meteor/ostrio:flow-router-extra';

import './addProduct.html';

import {
  Images
} from '../../../api/images/collections';
import {
  Categories
} from '../../../api/categories/collections';


Template.addProduct.onCreated(function () {

  this.loading = new ReactiveVar(false)

  this.autorun(() => {
    this.subscribe('getImages');
    this.subscribe('getCategories');
    this.subscribe('getProducts');
  })

});


Template.addProduct.helpers({
  loading() {
    return Template.instance().loading.get()
  },
  allCategories() {
    return Categories.find();
  }
});


Template.addProduct.events({
  'submit #addProductForm'(event, template) {
    event.preventDefault();
    let target = event.target;
    let name = target.productName.value;
    let categoryId = target.productCategory.value;
    let description = target.productDescription.value;
    let isFeatured = target.productFeatured.value ? true : false;


    let file = document.getElementById('myFile').files[0]

    const upload = Images.insert({
      file,
      chunkSize: 'dynamic'
    }, false);

    upload.on('start', function () {
      template.loading.set(this);
    });

    upload.on('end', function (error, fileObj) {
      if (error) {
        alert(`Error during upload: ${error}`);
      } else {
        console.log(`File "${fileObj.name}" successfully uploaded`);

        let product = {
          name: name,
          description: description,
          isFeatured: isFeatured,
          categoryId: categoryId,
          imageId: fileObj._id,
          avgRating: 0
        }

        let goToPage = "/"
        if (product.isFeatured) goToPage="/products"

        insertProduct(product, goToPage);
      }
      template.loading.set(false);
    });


    upload.start();
  }
});


function insertProduct(product, goToPage) {
  Meteor.call('addProduct', product, function (error, success) {
    if (error) alert('error', error);
    if (success) FlowRouter.go(goToPage);
  });
}