import {
    Template
} from 'meteor/templating';
import {
    ReactiveVar
} from 'meteor/reactive-var';
import {
    FlowRouter
} from 'meteor/ostrio:flow-router-extra';

import {
    Reviews
} from '../../../api/reviews/collections'
import {
    Products
} from '../../../api/products/collections'



import './addReview.html'




Template.addReview.onCreated(function () {

    this.autorun(() => {
        this.subscribe('getProducts');
        this.subscribe('getReviews');
    });

    this.productId = new ReactiveVar(0);
});



Template.addReview.helpers({
    getProduct() {
        Template.instance().productId.set(FlowRouter.getParam('_id'))
        return Products.findOne({
            _id: Template.instance().productId.get()
        });
    }
});


Template.addReview.events({
    'submit #addReviewForm': function (event, template) {
        event.preventDefault();
        let target = event.target;
        let rating = target.reviewRating.value;
        let reviewContent = target.reviewContent.value;


        let review = {
            rating: parseInt(rating),
            text: reviewContent,
            productId : template.productId.get(),
            dateTime: new Date()
        }

        Meteor.call('addReview', review, function(error, success) { 
            if (error) alert('error', error);  
            if (success) FlowRouter.go(`/reviews/${template.productId.get()}`);  
        });

    }
});