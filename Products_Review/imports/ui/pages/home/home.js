import {
    Template
} from 'meteor/templating';
import {
    ReactiveVar
} from 'meteor/reactive-var';
import {
    FlowRouter
} from 'meteor/ostrio:flow-router-extra';

import './home.html'

import {
    Images
} from '../../../api/images/collections';
import {
    Products
} from '../../../api/products/collections';
import {
    ProductsReviews
}
from '../../../api/productsReviews/collections';



Template.home.onCreated(function () {

    this.autorun(() => {
        this.subscribe('getProducts');
        this.subscribe('getImages');

        if (Products.find().count()) {
            ProductsReviews.remove()
            Products.find().map((elem) => {
                Meteor.call('getCount', elem._id, (err, res) => {
                    if (err) alert(err)
                    if (res != undefined) {
                        console.log(res)
                        ProductsReviews.insert({
                            productId: elem._id,
                            reviewCount: res
                        })
                    }
                });
            });
        }
    });

});


Template.home.helpers({
    loggedIn() {
        return Meteor.userId();
    },
    allProducts() {
        return Products.find({
            isFeatured: false
        });
    },
    featuredProducts() {
        return Products.find({
            isFeatured: true
        });
    },
    getImage() {
        return Images.findOne({
            _id: this.imageId
        })?.link();
    },
    getReviewsCountEach() {

        let eachCount = ProductsReviews.findOne({
            productId: this._id
        });

        return eachCount?.reviewCount
    },
    getStarFull() {
        let rating = this.avgRating;
        return StarFiller(rating)
    },
    getStarEmpty() {
        let rating = this.avgRating;
        return StarFiller((5 - rating))
    },
    getRouterName() {
        return FlowRouter.getRouteName();
    }
});


// =========================== RETURNS ARRAY OF STARS OF RATING
function StarFiller(rating = 0) {
    let arr = new Array(rating);

    for (let index = 0; index < rating; index++) {
        arr[index] = 1;
    }

    return arr;
}