import {
    Template
} from 'meteor/templating';
import {
    ReactiveVar
} from 'meteor/reactive-var';
import {
    FlowRouter
} from 'meteor/ostrio:flow-router-extra';

import './category.html'

import {
    Images
} from '../../../api/images/collections';
import {
    Products
} from '../../../api/products/collections';
import {
    ProductsReviews
}
from '../../../api/productsReviews/collections';


Template.category.onCreated(function () {

    this.categoryId = new ReactiveVar(0);

    this.autorun(() => {
        this.subscribe('getProducts');
        this.subscribe('getImages');

        this.categoryId.set(FlowRouter.getParam('_id'))
        console.log(this.categoryId.get())

        if (Products.find().count()) {
            ProductsReviews.remove()
            Products.find().map((elem) => {
                Meteor.call('getCount', elem._id, (err, res) => {
                    if (err) alert(err)
                    if (res != undefined) {
                        console.log(res)
                        ProductsReviews.insert({
                            productId: elem._id,
                            reviewCount: res
                        })
                    }
                });
            });
        }

    });
});


Template.category.helpers({
    loggedIn(){
        return Meteor.userId();
    },
    allProductsCategory() {
        return Products.find({
            categoryId: Template.instance().categoryId.get()
        });
    },
    getImage() {
        return Images.findOne({
            _id: this.imageId
        })?.link();
    },
    getReviewsCountEach() {

        let eachCount = ProductsReviews.findOne({
            productId: this._id
        });

        return eachCount?.reviewCount
    },
    getStarFull() {
        let rating = this.avgRating;
        return StarFiller(rating)
    },
    getStarEmpty() {
        let rating = this.avgRating;
        return StarFiller((5 - rating))
    }
});




// =========================== RETURNS ARRAY OF STARS OF RATING
function StarFiller(rating = 0) {
    let arr = new Array(rating);

    for (let index = 0; index < rating; index++) {
        arr[index] = 1;
    }

    return arr;
}