import {
    FlowRouter
} from 'meteor/ostrio:flow-router-extra';

import '../../ui/layouts/layout'

import '../../ui/pages/addProduct/addProduct'
import '../../ui/pages/addReview/addReview'
import '../../ui/pages/home/home'
import '../../ui/pages/reviews/reviews'
import '../../ui/pages/category/category'


FlowRouter.triggers.enter([trackRouteEntry], {only: ["addProduct"]});


FlowRouter.route('/', {
    name: 'home',
    action() {
        this.render('MainLayout', {
            main: 'home'
        });
    }
});

FlowRouter.route('/products', {
    name: 'products',
    action() {
        this.render('MainLayout', {
            main: 'home'
        });
    }
});

FlowRouter.route('/addProduct', {
    name: 'addProduct',
    action() {
        this.render('MainLayout', {
            main: 'addProduct'
        });
    }
});

FlowRouter.route('/addReview/:_id', {
    name: 'addReview',
    action(params) {
        this.render('MainLayout', {
            main: 'addReview'
        });
    }
});

FlowRouter.route('/reviews/:_id', {
    name: 'reviews',
    action(params) {
        this.render('MainLayout', {
            main: 'reviews'
        });
    }
});

FlowRouter.route('/category/:_id', {
    name: 'category',
    action(params) {
        this.render('MainLayout', {
            main: 'category'
        });
    }
});




function trackRouteEntry(context, redirect) {
    if (!Meteor.userId()) {
        redirect('/');
    }
}