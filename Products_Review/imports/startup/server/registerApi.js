// ===================  CATEGORIES
import '../../api/categories/collections'
import '../../api/categories/methods'
import '../../api/categories/publications'


//====================== PRODUCTS
import '../../api/products/collections'
import '../../api/products/methods'
import '../../api/products/publications'



// ===================== REVIEWS
import '../../api/reviews/collections'
import '../../api/reviews/methods'
import '../../api/reviews/publications'



// ===================== IMAGES
import '../../api/images/collections'
import '../../api/images/publications'



// ===================== ProductsReviews
import '../../api/productsReviews/collections'

